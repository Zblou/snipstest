#!/usr/bin/env python2
# -*- coding: utf-8 -*-

from snipsTools import SnipsConfigParser
from hermes_python.hermes import Hermes
from hermes_python.ontology import *
import io

CONFIG_INI = "config.ini"

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))

class Template(object):
    """Class used to wrap action code with mqtt connection
        
        Please change the name refering to your application
    """

    def __init__(self):
        # get the configuration if needed
        try:
            self.config = SnipsConfigParser.read_configuration_file(CONFIG_INI)
        except :
            self.config = None

        # start listening to MQTT
        self.start_blocking()
        
    # --> Sub callback function, one per intent
    def closeShutter(self, hermes, intent_message):
        # terminate the session first if not continue
        hermes.publish_end_session(intent_message.session_id, "Okay, I'll close those shutters.")
        
        # action code goes here...
        print '[Received] intent: {}'.format(intent_message.intent.intent_name)

        # if need to speak the execution result by tts
        hermes.publish_start_session_notification(intent_message.site_id,
                                                    "Action 1 has been done")

    def intent_openShutter(self, hermes, intent_message):
        sentence = "Shutters opened !"
        # terminate the session first if not continue
        hermes.publish_end_session(intent_message.session_id, sentence)

        # action code goes here...
        print '[Received] intent: {}'.format(intent_message.intent.intent_name)
        hermes.publish_continue_session(intent_message.session_id, sentence, [intent_openShutter])

        # if need to speak the execution result by tts
        hermes.publish_start_session_notification(intent_message.site_id, 
                                                    "Action 2 has been done")

    def intent_endSession(self, hermes, intent_message):
        # terminate the session first if not continue
        hermes.publish_end_session(intent_message.session_id, "Bye bye !")

        # action code goes here...
        print '[Received] intent: {}'.format(intent_message.intent.intent_name)

        # if need to speak the execution result by tts
        hermes.publish_start_session_notification(intent_message.site_id, 
                                                    "Action 3 has been done")
        

    # --> Master callback function, triggered everytime an intent is recognized
    def master_intent_callback(self,hermes, intent_message):
        coming_intent = intent_message.intent.intent_name
        if coming_intent == 'intent_closeShutter':
            self.closeShutter(hermes, intent_message)
        if coming_intent == 'intent_openShutter':
            self.intent_openShutter(hermes, intent_message)
        if coming_intent == 'intent_endSession':
            self.intent_endSession(hermes, intent_message)


    # --> Register callback function and start MQTT
    def start_blocking(self):
        with Hermes(MQTT_ADDR) as h:
            h.subscribe_intents(self.master_intent_callback).start()

if __name__ == "__main__":
    Template()