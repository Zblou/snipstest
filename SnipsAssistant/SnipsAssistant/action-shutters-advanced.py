#!/usr/bin/env python2
from hermes_python.hermes import Hermes

MQTT_IP_ADDR = "localhost"
MQTT_PORT = 1883
MQTT_ADDR = "{}:{}".format(MQTT_IP_ADDR, str(MQTT_PORT))

intent_closeShutter = "intent_closeShutter"
intent_openShutter = "intent_openShutter"
intent_endSession = "intent_endSession"

def closeShutter(hermes, intent_message):
   sentence = "Shutters closed yeah."
   print "Shutters correctly closed"
   hermes.publish_continue_session(intent_message.session_id, sentence, [intent_closeShutter])

def openShutter(hermes, intent_message):
    sentence = "Shutters opened yeah."
    print "Shutters opened"
    hermes.publish_continue_session(intent_message.session_id, sentence, [intent_openShutter])

def endSession(hermes, intent_message):
    sentence = "Bye Tanguy."
    print "Bye bye"
    hermes.publish_continue_session(intent_message.session_id, sentence, [intent_endSession])

with Hermes(MQTT_ADDR) as h:
    h.subscribe_intent(intent_closeShutter, closeShutter)
    h.subscribe_intent(intent_openShutter, openShutter)
    h.subscribe_intent(intent_endSession, endSession)


sentence2 = "Closing Snips"
print "Prout"
#hermes.publish_end_session(intent_message.session_id, sentence2)

##modif intent variables